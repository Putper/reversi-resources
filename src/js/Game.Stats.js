Game.Stats = (() =>
{
    let _stateMap =
    {
        // ChartJS objects
        discsCountChart: null,
        conqueredChart: null,
    }

    /**
     * init the chart which shows the amount of discs each player had on the board per turn
     * @param {Element} element reference to the canvas element
     * @param {string} player1Name 
     * @param {string} player2Name 
     */
    const initDiscCount = (element, player1Name, player2Name) =>
    {
        const data = {
            labels: [],
            datasets: [
                {
                    label: player1Name,
                    data: [],
                    borderColor: '#000',
                    backgroundColor: '#FFFFFF',
                    borderWidth: 2,
                    borderRadius: Number.MAX_VALUE,
                    borderSkipped: 'bottom',
                },
                {
                    label: player2Name,
                    data: [],
                    backgroundColor: '#000',
                    borderRadius: Number.MAX_VALUE,
                    borderSkipped: 'bottom',
                },
            ]
        }

        _stateMap.discsCountChart = new Chart(element, {
            type: 'bar',
            data,
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Discs on Board'
                    }
                }
            }
        })
    }

    /**
     * init the chart which shows the amount of discs each player has conquered from the other per turn
     * @param {Element} element 
     * @param {string} player1Name 
     * @param {string} player2Name 
     */
    const initConqueredChart = (element, player1Name, player2Name) =>
    {
        const data = {
            labels: [],
            datasets: [
                {
                    label: player1Name,
                    data: [],
                    borderColor: '#00e0f5',
                    backgroundColor: '#FFFFFF',
                    tension: 0.4,
                },
                {
                    label: player2Name,
                    data: [],
                    borderColor: '#000000',
                    backgroundColor: '#000000',
                    tension: 0.4,
                }
            ]
        }

        _stateMap.conqueredChart = new Chart(element, {
            type: 'line',
            data,
            options: {
                responsive: true,
                spanGaps: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Conquered Discs'
                    }
                }
            }
        })
    }

    /**
     * update all charts with given turndata
     * @param {[Object]} turnData array of TurnDatum objects as given by the API
     */
    const updateFromTurnData = (turnData) =>
    {
        _updateDiscsCount(turnData)
        _updateConqueredChart(turnData)
    }

    /**
     * update the data of the discs count chart
     * @param {[Object]} turnData array of TurnDatum objects as given by the API
     */
    const _updateDiscsCount = (turnData) =>
    {   
        // remove old data, start with start turn
        _stateMap.discsCountChart.data.labels = ['Start']
        _stateMap.discsCountChart.data.datasets[0].data = [2]
        _stateMap.discsCountChart.data.datasets[1].data = [2]

        // add played turns
        turnData.forEach(data =>
        {
            // update the discs count chart
            _stateMap.discsCountChart.data.labels.push("Turn " + (data.turn + 1))
            _stateMap.discsCountChart.data.datasets[0].data.push(data.player1OwnedDiscs)
            _stateMap.discsCountChart.data.datasets[1].data.push(data.player2OwnedDiscs)
        })
        
        _stateMap.discsCountChart.update()
    }

    /**
     * update the data of the conquered chart
     * @param {[Object]} turnData array of TurnDatum objects as given by the API
     */
    const _updateConqueredChart = (turnData) =>
    {
        // remove old data, start with start turn
        _stateMap.conqueredChart.data.labels = ["Turn 0"]
        _stateMap.conqueredChart.data.datasets[0].data = [0]
        _stateMap.conqueredChart.data.datasets[1].data = [0]

        // add played turns
        turnData.forEach(data =>
        {
            _stateMap.conqueredChart.data.labels.push(data.turn + 1)

            // add conquered discs to the right player
            if(data.isPlayer2)
            {
                _stateMap.conqueredChart.data.datasets[0].data.push(null)
                _stateMap.conqueredChart.data.datasets[1].data.push(data.conqueredDiscs)
            }
            else
            {
                _stateMap.conqueredChart.data.datasets[0].data.push(data.conqueredDiscs)
                _stateMap.conqueredChart.data.datasets[1].data.push(null)
            }
        })

        _stateMap.conqueredChart.update()
    }

    return {
        initDiscCount,
        initConqueredChart,
        updateFromTurnData,
    }
})()
