Game.PlayerList = (() =>
{
    /**
     * element to place the playerlist in
     * @type {null|Element}
     * @private
     */
    let _element = null

    /**
     * @type {Game.Data.GameState}
     * @private
     */
    let _playerColour = null

    /**
     * name of white player
     * @type {null|string}
     * @private
     */
    let _whitePlayerName = null

    /**
     * name of black player
     * @type {null|string}
     * @private
     */
    let _blackPlayerName = null

    /**
     *
     * @param {Element} element element to create the player list in
     * @param {string} whitePlayerName
     * @param {string} blackPlayerName
     * @param {Game.Data.GameState} playerColour
     */
    const init = (element, whitePlayerName, blackPlayerName, playerColour) =>
    {
        _element = element
        _playerColour = playerColour
        _whitePlayerName = whitePlayerName
        _blackPlayerName = blackPlayerName
    }

    /**
     * Draw the playerlist
     * @param {Game.Data.GameState} playerTurn who's turn it is
     */
    const draw = (playerTurn) =>
    {
        if(_element == null)
            return
        // remove old playerlist
        Helpers.removeAllFromContainerWithClass(_element, "playerlist")

        // add new playerlist
        const html = Game.Template.getParsed("game.playerlist", {
            whiteName: _whitePlayerName,
            blackName: _blackPlayerName,
            playerIsWhite: (_playerColour === Game.Data.GameState.White),
            whitesTurn: (playerTurn === Game.Data.GameState.White),
        })
        _element.insertAdjacentHTML('beforeend', html)
    }

    return {
        init,
        draw
    }
})()