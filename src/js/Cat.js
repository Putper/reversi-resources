const Cat = (() =>
{
    /**
     * load cat template in elements that match the given querySelector
     * @param {string} querySelector
     */
    const initTemplates = (querySelector) =>
    {
        const elements = document.querySelectorAll(querySelector)
        Array.from(elements).forEach(element =>
        {
            // get cat image
            getCatImageUrl().then(imageUrl =>
            {
                // get cat fact
                getCatFact().then(catFact =>
                {
                    // parse and show template
                    const data = {catImageUrl: imageUrl, catFact: catFact}
                    const template = Game.Template.getParsed('cat.cat', data)
                    element.insertAdjacentHTML('beforeend', template)
                })
            })
        })
    }

    /**
     * get a random cat image
     * @returns {Promise<string>}
     */
    const getCatImageUrl = async() =>
    {
        const url = "https://api.thecatapi.com/v1/images/search?size=full"
        const catImage = await Game.Data.get(url)
        if(catImage == null || !Array.isArray(catImage) || Array.from(catImage).length < 1 || catImage[0].url == undefined)
        {
            throw "Failed getting cat image"
        }
        return catImage[0].url
    }

    /**
     * get a random cat fact
     * @returns {Promise<string>}
     */
    const getCatFact = async() =>
    {
        const url = "https://catfact.ninja/fact"
        const fact = await Game.Data.get(url)
        if(fact == null || fact.fact == undefined)
        {
            throw "Failed getting cat fact"
        }
        return fact.fact
    }

    return {
        getCatImageUrl,
        getCatFact,
        initTemplates
    }
})()

