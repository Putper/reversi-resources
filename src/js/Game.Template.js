Game.Template = (() =>
{
    /**
     * Get reference to a template function.
     *
     * directories can be specified with a dot in the name parameter.
     * For example if your structure is game/parts/disc.hbs,
     * then name should be "game.parts.disc".
     * @param {string} name template name
     * @returns {function|null} template
     */
    const get = (name) =>
    {
        // ensure spa_templates is defined
        if(spa_templates === undefined)
            return null

        name = "src.templates." + name
        const paths = name.split('.')

        let result = spa_templates
        for(let i=0; i<paths.length; ++i)
        {
            const path = paths[i]
            if(result[path] === undefined)
                return null
            result = result[path]
        }

        return result
    }


    /**
     * Get HTML for the given template with given data.
     *
     * directories can be specified with a dot in the name parameter.
     * For example if your structure is game/parts/disc.hbs,
     * then name should be "game.parts.disc".
     * @param {string} name
     * @param {Object} data
     * @returns {null|string}
     */
    const getParsed = (name, data) =>
    {
        const template = get(name)
        if(template === null) {
            return null
        }
        return template(data)
    }

    return {
        get,
        getParsed
    }
})()
