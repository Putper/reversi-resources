document.addEventListener('DOMContentLoaded', (event) =>
{
    // only development page has this id
    // would be nicer to compile this to it's own .js file, and load that in exclusively on the development page
    let main = document.getElementById("main-development")
    if(!main)
        return

    let remindbutton = document.getElementById("remindbutton")
    if(remindbutton)
    {
        remindbutton.addEventListener("click", () =>
        {
            new FeedbackWidget("Vergeet geen water te drinken!", "success").show().log()
        })
    }

    const board = document.getElementsByClassName("board__container")[0]
    const playerList = document.getElementsByClassName("playerlist__container")[0]
    Game.PlayerList.init(playerList, "Player 1", "Player 2", 1)

    Game.Data.init(Game.Data.Environment.Development, "http://example.com/api", "my-api-token")
    Game.init('my-game-token', 1, 'player-1-token')
    Game.initBoard(board)
    Cat.initTemplates(".catcontainer")
    
    // init charts
    Game.Stats.initDiscCount(document.getElementById("discs-count-chart"), "Player 1", "Player 2")
    Game.Stats.initConqueredChart(document.getElementById("conquered-chart"), "Player 1", "Player 2")
})
