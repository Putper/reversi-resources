Game.Waiting = (() =>
{
    /**
     * @param apiUrl
     * @param gameToken
     */
    const init = (gameToken) =>
    {
        Game.API.init(gameToken)
        window.setInterval(_checkGameReady, 2000)
    }

    /**
     * check if the game is ready
     * @returns {Promise<void>}
     * @private
     */
    const _checkGameReady = async () =>
    {
        const isReady = await Game.API.gameIsReady()
        if(isReady)
            _onGameReady()
    }

    /**
     * reload page when game is ready
     * @private
     */
    const _onGameReady = () =>
    {
        window.location.reload()
    }

   return {
        init
   }
})()
