const Game = (() =>
{
    /**
     * config state
     * @type {{finishUrl: string, gameToken: null|string, playerColour: Game.Data.GameState, playerToken: string|null}}
     */
    let configMap =
    {
        finishUrl: null,
        gameToken: null,
        playerColour: 0,
        playerToken: null
    }

    /**
     * @type {{gameState: Game.Data.GameState, waitingForOpponent: boolean, game: null|Object}}
     */
    let stateMap =
    {
        gameState: 0, // who's turn it is
        waitingForOpponent: false,
        game: null
    }

    /**
     * store the update interval so it can be cleared later
     * @type {number|null}
     * @private
     */
    let _updateInterval = null

    /**
     * Initialise game
     * @param {string} gameToken gametoken used to get game data
     * @param {Game.Data.GameState} playerColour
     * @param {string} playerToken token of currently playing player
     * @param {string} jwtToken token used to communicate with the API
     */
    const init = (gameToken, playerColour, playerToken) =>
    {
        configMap.gameToken = gameToken
        configMap.playerColour = playerColour
        configMap.playerToken = playerToken
        configMap.finishUrl = `${location.protocol}//${location.host}${location.pathname}/Finish`
        Game.API.init(configMap.gameToken)

        window.setTimeout(_init, 0)
        // start game loop
        _updateInterval = window.setInterval(_update, 2000)
    }

    /**
     * Set initial data
     * @returns {Promise<void>}
     * @private
     */
    const _init = async () =>
    {
        await _updateGameState()
        _drawPlayerList()
        stateMap.waitingForOpponent = (stateMap.gameState !== configMap.playerColour)
        // if game ended, show end screen (Unspecified = no-one's turns = a finished game)
        if(stateMap.gameState === Game.Data.GameState.Unspecified)
            await _onFinished()
    }

    /**
     * @param {Element} element element to create the board in
     */
    const initBoard = (element) =>
    {
        Game.Reversi.init(
            element,
            configMap.gameToken
        )
        // initial drawings
        _drawPlayerList()
        window.setTimeout(_updateGameWithNewData, 0)
    }

    /**
     * do move at given location
     * @param row
     * @param column
     * @returns {Promise<void>}
     */
    const doMove = async (row, column) =>
    {
        // cant place if not your turn
        if(configMap.playerColour !== stateMap.gameState)
            return
        // cant place on already placed discs
        if(Game.Reversi.tileHasDisc(row, column))
            return

        const moveResult = await Game.API.doMove(row, column, configMap.playerToken)
        if(moveResult.success === false)
        {
            new FeedbackWidget("This is an invalid move", "error").hideDeclineButton().show()//.log()
            return
        }

        Game.Reversi.placeDisc(row, column, configMap.playerColour)
        stateMap.gameState = _getOpponentsColour()
        stateMap.waitingForOpponent = true
        _updateGame(moveResult.game.board, moveResult.game.turnData)
        _drawPlayerList()

        if(moveResult.game.isFinished)
            await _onFinished(moveResult.game.winner === configMap.playerColour.valueOf())
    }

    /**
     * main game loop
     * @returns {Promise<void>}
     * @private
     */
    const _update = async () =>
    {
        await _updateGameState()
        if(stateMap.waitingForOpponent)
        {
            // if the opponent just did their move
            if(_isPlayersTurn())
            {
                _drawPlayerList()
                await _updateGameWithNewData()

                stateMap.waitingForOpponent = false
            }
        }
        // Unspecified = no-one's turns = a finished game
        if(stateMap.gameState === Game.Data.GameState.Unspecified)
            await _onFinished()
    }

    /**
     * get who's turn it is and store it in the statemap
     * @returns {Promise<void>}
     * @private
     */
    const _updateGameState = async () =>
    {
        stateMap.gameState = await Game.API.getGameState()
    }

    /**
     * Get game data from API and render it
     * @returns {Promise<void>}
     * @private
     */
    const _updateGameWithNewData = async () =>
    {
        stateMap.game = await Game.API.getGame()
        board = stateMap.game.board
        turnData = stateMap.game.turnData

        _updateGame(board, turnData)
    }

    /**
     * update the visuals based on the given data
     * @param {string} board string of numbers representing the game board state
     * @param {[Object]} turnData array of objects, each representing a turn
     * @private
     */
    const _updateGame = (board, turnData) =>
    {
        // update board
        let boardElement = Game.Reversi.updateBoard(board)
        _registerBoardClickEvents(boardElement)

        Game.Stats.updateFromTurnData(turnData)
    }

    /**
     * @param {Element} board
     * @private
     */
    const _registerBoardClickEvents = (board) =>
    {
        const tiles = board.getElementsByClassName("board__tile")
        Array.from(tiles).forEach(tile =>
        {
            tile.addEventListener("click", () =>
            {
                let row = tile.dataset.row
                let column = tile.dataset.column
                doMove(row, column)
            })
        })
    }

    /**
     * If it's the current players' turn or not
     * @returns {boolean}
     * @private
     */
    const _isPlayersTurn = () =>
    {
        return stateMap.gameState === configMap.playerColour
    }

    /**
     * Get colour of the opponent
     * @returns {Game.Data.GameState}
     * @private
     */
    const _getOpponentsColour = () =>
    {
        return configMap.playerColour === Game.Data.GameState.White
            ? Game.Data.GameState.Black : Game.Data.GameState.White
    }

    /**
     * draw the player list
     * @private
     */
    const _drawPlayerList = () =>
    {
        Game.PlayerList.draw(stateMap.gameState)
    }

    /**
     * @param {boolean|null} won if you won
     * @private
     */
    const _onFinished = async (won=null) =>
    {
        clearInterval(_updateInterval)
        window.location.replace(configMap.finishUrl)
        // // get winner if not given
        // if(won === null)
        // {
        //     const game = await Game.API.getGame()
        //     won = game.winner === configMap.playerColour.valueOf()
        // }
        // if(won)
        //     new FeedbackWidget("You won!", "success").show()
        // else
        //     new FeedbackWidget("You lost, better luck next time!", "error").show()
    }

    return {
        init,
        initBoard
    }
})()
