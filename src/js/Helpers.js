const Helpers = (() =>
{

    const fadeIn = (element, afterFade=null) =>
    {
        element.classList.add("helpers__fade")
        element.classList.remove("helpers__fade--hidden")
        element.classList.add('helpers__fade--invisible')
        window.setTimeout(() => {
            element.classList.remove('helpers__fade--invisible')
            // call callback
            if(afterFade != null) {
                afterFade()
            }
        }, 5)
    }


    const fadeOut = (element, afterFade=null) =>
    {
        element.classList.add("helpers__fade")
        element.classList.add('helpers__fade--invisible')
        window.setTimeout(() => {
            element.classList.remove('helpers__fade--invisible')
            element.classList.add('helpers__fade--hidden')
            // call callback
            if(afterFade != null) {
                afterFade()
            }
        }, 1000)
    }


    /**
     * Remove all elements with a given classname from a given container
     * @param container parent to remove child elements from
     * @param className remove all child elements with this classname
     */
    const removeAllFromContainerWithClass = (container, className) =>
    {
        const elements = container.getElementsByClassName(className)
        Array.from(elements).forEach(element => {container.removeChild(element)})
    }


    return {
        fadeIn,
        fadeOut,
        removeAllFromContainerWithClass
    }
})()
