/**
 * Manages the Reversi game board
 * A whole board can be drawn or discs can be added&removed one by one
 */
Game.Reversi = (() =>
{
    /**
     * @type {Element}
     */
    let _gameContainer

    /**
     * Only used as a seed
     * @type {string}
     */
    let _gameToken

    /**
     * @param {HTMLElement} gameContainer element to insert board in
     * @param {string} gameToken
     */
    const init = (gameContainer, gameToken) =>
    {
        _gameContainer = gameContainer
        _gameToken = gameToken
    }

    /**
     * Update the board with the given board string
     * @param board
     * @return {Element}
     */
    const updateBoard = (board) =>
    {
        // remove old board
        Helpers.removeAllFromContainerWithClass(_gameContainer, "board")
        // create new board
        const boardHTML = _getBoardHTML(board)
        _gameContainer.insertAdjacentHTML('beforeend', boardHTML)
        return _gameContainer.lastElementChild
    }

    /**
     * place disc in given location
     * @param {number} row starts at 0
     * @param {number} column starts at 0
     * @param {Game.Data.GameState} colour
     */
    const placeDisc = (row, column, colour) =>
    {
        const tile = _getTile(row, column)
        // place
        const disc = _createDisc(colour)
        tile.insertAdjacentHTML("beforeend", disc)
        // fade out
        const discElement = tile.lastElementChild
        Helpers.fadeIn(discElement)
    }

    /**
     * remove disc from given location
     * @param {number} row
     * @param {number} column
     * @return {boolean} if it contained a disc or not
     */
    const removeDisc = (row, column) =>
    {
        let tile = _getTile(row, column)
        let discs = tile.getElementsByClassName("board__disc")
        if(discs.length == 0) {
            return false
        }
        Array.from(discs).forEach(disc =>
        {
            Helpers.fadeOut(disc, () =>
            {
                disc.remove()
            })
        })
        return true
    }

    /**
     * check if given location already has a disc
     * @param {number} row
     * @param {number} column
     * @returns {boolean} if given location already has a disc
     */
    const tileHasDisc = (row, column) =>
    {
        const tile = _getTile(row, column)
        return tile.children.length !== 0
    }

    /**
     * get tile on board from given location
     * @param {number} row
     * @param {number} column
     * @returns {Element}
     * @private
     */
    const _getTile = (row, column) =>
    {
        return _gameContainer.querySelector(`[data-row="${row}"][data-column="${column}"]`)
    }

    /**
     * @param {string} board
     * @param {string} gameToken
     * @return {string}
     */
    const _getBoardHTML = (board) =>
    {
        const params = {
            boardHeight: 1,
            tiles: []
        }
        // fill parameters
        let column = 0
        let row = 0
        for(let i=0; i<board.length; ++i)
        {
            // count height of board with 3, because each 3 represents a new row
            if(board[i] == 3) {
                params.boardHeight++
                row++
                column = 0
                continue
            }

            // add a disc if it has one
            let disc = null
            if(board[i] != 0)
                disc = (board[i] == 2) ? "black" : "white"
            // get random background
            let colour = i % 2 == 0 ? "light" : "medium"
            const background = _getRandomBackground(colour, _gameToken + i)

            // add tile to tiles
            params.tiles.push({
                row: row,
                column: column,
                backgroundClass: background,
                hasDisc: (disc != null),
                discIsBlack: (disc == "black")
            })
            column++
        }

        return Game.Template.getParsed('game.board', params)
    }

    /**
     * colour = black|white
     * @param {Game.Data.GameState} colour
     * @returns {string}
     * @private
     */
    const _createDisc = (colour) =>
    {
        const isBlack = (colour === Game.Data.GameState.Black)
        return Game.Template.getParsed("game.disc", {black: isBlack})
    }

    /**
     * get a random tile background
     * using a seed for randomisation
     * @param {string} colour
     * @param {string} seed
     * @returns {string}
     * @private
     */
    const _getRandomBackground = (colour, seed) =>
    {
        // get a random number for the tile's background
        let rng = new Math.seedrandom(seed)
        let randomNumber = Math.floor(rng() * 7)
        let number = 1
        if(randomNumber >= 4 && randomNumber <= 6)
            number = 2
        else if(randomNumber == 0)
            number = 3
        return "board__tile--" + colour + number
    }

    return {
        init,
        updateBoard,
        placeDisc,
        removeDisc,
        tileHasDisc
    }
})()
