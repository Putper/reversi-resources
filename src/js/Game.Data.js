Game.Data = (() =>
{
    /**
     * Environment enum
     * @type {Readonly<{Production: number, Development: number}>}
     */
    const Environment = Object.freeze({Development: 0, Production: 1})

    /**
     * GameState enum
     * @type {Readonly<{White: number, Unspecified: number, Black: number}>}
     */
    const GameState = Object.freeze({Unspecified: 0, White: 1, Black: 2})

    let configMap =
    {
        apiToken: null,
        mock: [
            {
                url: "*/turn*",
                data: 2,
            },
            {
                url: "*/api/game/*",
                data:
                {
                    "token": "game-token",
                    "description": "Game from Mock data",
                    "player1Token": "player-1-token",
                    "player2Token": "player-2-token",
                    "board": "00000000300000000300000000300012000300021000300000000300000000300000000",
                    "turn": 2,
                    "isFinished": false,
                    "winner": 0,
                    "turnData": [
                        {
                        "gameToken": "game-token",
                        "turn": 0,
                        "isPlayer2": false,
                        "conqueredDiscs": 1,
                        "player1OwnedDiscs": 4,
                        "player2OwnedDiscs": 1
                        },
                        {
                        "gameToken": "game-token",
                        "turn": 1,
                        "isPlayer2": true,
                        "conqueredDiscs": 1,
                        "player1OwnedDiscs": 3,
                        "player2OwnedDiscs": 3
                        },
                        {
                        "gameToken": "game-token",
                        "turn": 2,
                        "isPlayer2": false,
                        "conqueredDiscs": 1,
                        "player1OwnedDiscs": 5,
                        "player2OwnedDiscs": 2
                        },
                        {
                        "gameToken": "game-token",
                        "turn": 3,
                        "isPlayer2": true,
                        "conqueredDiscs": 3,
                        "player1OwnedDiscs": 3,
                        "player2OwnedDiscs": 5
                        },
                        {
                        "gameToken": "game-token",
                        "turn": 4,
                        "isPlayer2": false,
                        "conqueredDiscs": 1,
                        "player1OwnedDiscs": 5,
                        "player2OwnedDiscs": 4
                        },
                        {
                        "gameToken": "game-token",
                        "turn": 5,
                        "isPlayer2": true,
                        "conqueredDiscs": 1,
                        "player1OwnedDiscs": 4,
                        "player2OwnedDiscs": 6
                        },
                        {
                        "gameToken": "game-token",
                        "turn": 6,
                        "isPlayer2": false,
                        "conqueredDiscs": 1,
                        "player1OwnedDiscs": 6,
                        "player2OwnedDiscs": 5
                        }
                    ]
                }
            },
            {
                url: "*//api.openweathermap.org/data/2.5/weather*",
                data:
                {
                    main: {temp: 6}
                }
            },
            {
                url: "*//api.thecatapi.com/v1/images/search?size=full*",
                data:
                [
                    {
                        id: "MTc5MzIwNQ",
                        url: "https://cdn2.thecatapi.com/images/MTc5MzIwNQ.jpg",
                        // url: "/images/branding/logo.png",
                        width: 2800,
                        height: 2090
                    }
                ]
            },
            {
                url: "*//catfact.ninja/fact*",
                data:
                {
                    fact: "Cats have 32 individual muscles in each ear just to ignore you when you speak to them.",
                    length: 86
                }
            }
        ]
    }

    /**
     * @type {{environment: Game.Data.Environment}}
     */
    let stateMap =
    {
        environment: Environment.Production
    }

    /**
     * in production data is received from API,
     * in development data is received from mock data
     * @param environment {Game.Data.Environment}
     */
    const init = (environment, baseURL, apiToken) =>
    {
        configMap.apiToken = apiToken
        stateMap.environment = environment

        axios.defaults.baseURL = baseURL
        axios.defaults.headers.common['Authorization'] = `bearer ${apiToken}`
    }

    /**
     * get data from given url
     * @param url
     * @returns {Promise<any>}
     */
    const get = async (url) =>
    {
        if(stateMap.environment !== Environment.Production)
            return getMockData(url)

        const promise = axios.get(url)
        return await promise.then(response => response.data)
    }

    /**
     * put data to given url
     * @param url
     * @param data
     * @returns {Promise<any>}
     */
    const put = async (url, data) =>
    {
        if(stateMap.environment !== Environment.Production)
            return getMockData(url)

        const promise = axios.put(url, data)
        return await promise.then(response => response.data)
    }

    /**
     * get mock data for given url
     * @param {string} url
     * @returns {Promise<any>}
     */
    const getMockData = (url) =>
    {
        const mockData = configMap.mock.find(mock =>
        {
            let regex = wildcardToRegex(mock.url)
            return regex.test(url)
            //return mock.url === url
        })
        return new Promise((resolve, reject) =>
        {
            if(mockData === undefined)
                reject("No mockdata present for url: " + url)
            else
                resolve(mockData.data)
        })
    }

    /**
     * wildcards in strings should be *
     * @source https://gist.github.com/donmccurdy/6d073ce2c6f3951312dfa45da14a420f
     * @param {string} wildcard
     * @returns {RegExp}
     */
    const wildcardToRegex = (wildcard) =>
    {
        return new RegExp('^' + wildcard.split(/\*+/).map((s) =>
        {
            return s.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')
        }).join('.*') + '$')
    }

    return {
        init,
        get,
        put,
        Environment,
        GameState,
    }
})()
