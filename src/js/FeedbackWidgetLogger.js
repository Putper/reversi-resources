class FeedbackWidgetLogger
{
    /**
     * under which key in LocalStorage the logged fedbackitems are stored
     * @type {string}
     * @private
     */
    static _storageKey = "feedback_widget"


    /**
     * add given message in localstorage, to be remembered
     * @param {string} message
     * @param {string} type either "success" or "error"
     */
    static log(message, type)
    {
        let messages = this._getAll()

        // define object to add
        let messageObject = { message : message, type : type }

        // add & store
        messages.push(messageObject)
        this._save(messages)
    }

    /**
     * remove given from logs in LocalStorage
     * @param {string} message 
     * @param {string} type 
     */
    static remove(message, type)
    {
        let messages = this._getAll()

        let index = messages.findIndex(item => item.message == message && item.type == type)

        if(index == -1)
        {
            console.error(`Called FeedbackWidgetLogger.remove() on values not saved in log. For type "${type}" and message "${message}"`)
            return
        }

        messages.splice(index, 1)

        this._save(messages)
    }

    /**
     * 
     * @returns {FeedbackWidget[]}
     */
    static showAll()
    {
        let messages = this._getAll()

        let created = []
        messages.forEach(item =>
        {
            if(item.type === undefined || item.message === undefined)
                return

            const feedbackWidget = new FeedbackWidget(item.message, item.type)
            feedbackWidget.show()
            created.push(feedbackWidget)
        })
        return created
    }

    /**
     * retrieve all logged in LocalStorage
     * @returns {{ message : string, type : "success"|"error" }}
     */
    static _getAll()
    {
        // stored as 
        let logs = localStorage.getItem(this._storageKey)
        return (logs === null) ? [] : JSON.parse(logs)
    }

    /**
     * log given in LocalStorage
     * @param {[{message: string, type: string}]} messages 
     */
    static _save(messages)
    {
        window.localStorage.setItem(this._storageKey, JSON.stringify(messages))
    }

    // /**
    //  * remove all logs from localstorage
    //  */
    // static _removeAll()
    // {
    //     localStorage.removeItem(this._storageKey)
    // }
}
