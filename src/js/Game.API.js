Game.API = (() =>
{
    /**
     * @type {{gameToken: string|null}}
     */
    let configMap =
    {
        gameToken: null,
    }

    const init = (gameToken) =>
    {
        configMap.gameToken = gameToken
    }

    /**
     * get the current gamestate (who's turn it is)
     * @returns {Promise<int>}
     */
    const getGameState = async () =>
    {
        const url = "/api/game/" + configMap.gameToken + "/turn"
        const state = await Game.Data.get(url)
        // ensure is proper GameState
        if(Object.values(Game.Data.GameState).indexOf(state) < 0)
        {
            throw new Error("Did not receive a proper or known GameState value.")
        }
        return state
    }

    /**
     * @returns {Promise<boolean>}
     */
    const gameIsReady = async() =>
    {
        const url = `/api/game/${configMap.gameToken}/ready`
        const isReady = await Game.Data.get(url)
        return isReady
    }


    /**
     * Get game object from API
     * @returns {Promise<null|{token:string,description:string,board:string,turn:number,isFinished:boolean,winner:number}>}
     */
    const getGame = async () =>
    {
        const url = "/api/game/" + configMap.gameToken
        const game = await Game.Data.get(url)
        return game
    }


    /**
     * Do move at API
     * @param row
     * @param column
     * @param playerToken
     * @returns {Promise<{success: boolean, game:null|{token:string,description:string,board:string,turn:number,isFinished:boolean,winner:number}}>}
     */
    const doMove = async (row, column, playerToken) =>
    {
        const url = "/api/game/" + configMap.gameToken
        const data = {
            row: row,
            column: column,
            playerToken: playerToken
        }
        return await Game.Data.put(url, data)
    }


    // /**
    //  * returns temperature in Zwolle
    //  * @returns {Promise<int>}
    //  */
    // const getWeather = async () =>
    // {
    //     const url = "http://api.openweathermap.org/data/2.5/weather?q=zwolle&apikey=?"
    //     const weather = await Game.Data.get(url)
    //     if(weather.main === undefined || weather.main.temp === undefined)
    //     {
    //         throw "Failed getting weather"
    //     }
    //     return weather.main.temp
    // }

    return {
        init,
        getGameState,
        gameIsReady,
        getGame,
        //getWeather,
        doMove,
    }
})()
