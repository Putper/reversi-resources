class FeedbackWidget
{
    /**
     * container element that defines the widget
     * @type {HTMLDivElement}
     * @private
     */
    _element

    /**
     * message content of widget
     * @type {string}
     * @private
     */
    _message

    /**
     * either "success" or "error"
     * @type {string}
     * @private
     */
    _type
    /**
     * Sets the right colour css-class based on set type
     * @param {string} type must be "success" or "error"
     */
    set type(type)
    {
        this._type = type
        // remove old
        this._element.classList.remove("feedbackwidget--success")
        this._element.classList.remove("feedbackwidget--error")
        // add new
        let typeClass = type === "success" ? "feedbackwidget--success" : "feedbackwidget--error"
        this._element.classList.add(typeClass)
    }


    /**
     * Automatically creates a hidden feedbackwidget,
     * can be shown with .show() method
     * @param {string} message
     * @param {string} type
     */
    constructor(message, type)
    {
        // container
        this._element = document.createElement("article")
        this._element.classList.add("feedbackwidget")
        this._element.setAttribute("role", "alert")
        this._type = type
        this._element.classList.add('feedbackwidget--hidden')
        this._element.classList.add("alert")

        // message text
        let text = document.createElement("p")
        text.classList.add("feedbackwidget__text")
        text.innerHTML = message
        this._message = message
        this._element.appendChild(text)

        // close button
        let closeButton = this.createCloseButton()
        text.appendChild(closeButton)

        // accept button
        let acceptButton = document.createElement("button")
        acceptButton.innerHTML = "Akkoord"
        acceptButton.type = "button"
        acceptButton.classList.add('feedbackwidget__acceptbutton', 'btn', 'btn-success')

        acceptButton.addEventListener("click", () => { this.onClose() })
        this._element.appendChild(acceptButton)
        this.infiniteTada(acceptButton)

        // decline button
        let declineButton = document.createElement("button")
        declineButton.innerHTML = "Weigeren"
        declineButton.type = "button"
        declineButton.classList.add('feedbackwidget__declinebutton', 'btn', 'btn-danger')
        
        declineButton.addEventListener("click", () => { this.onClose() })
        this._element.appendChild(declineButton)

        // ensure alerts container exists
        let alertsContainer = document.getElementById("alerts")
        if(alertsContainer === null) {
            alertsContainer = document.createElement("section")
            alertsContainer.id = "alerts"
            document.body.prepend(alertsContainer)
        }
        // add to alerts container in DOM
        alertsContainer.prepend(this._element)
    }


    /**
     * Get element of this widget
     * @returns {*}
     */
    get element()
    {
        return this._element
    }


    /**
     * Create a close button visualised with an X
     * @return {HTMLButtonElement}
     */
    createCloseButton()
    {
        // close button
        let button = document.createElement("button")
        button.type = "button"
        button.classList.add("btn-close", "feedbackwidget__closebutton")
        button.setAttribute('aria-label', 'Close')
        // hide on click
        button.addEventListener("click", () => { this.onClose() })
        return button
    }

    /**
     * hide the close button
     * @returns {FeedbackWidget}
     */
    hideDeclineButton()
    {
        let declineButton = this._element.querySelector(".feedbackwidget__declinebutton")
        declineButton.classList.add("d-none")
        return this
    }


    /**
     * show widget with fade animation
     * @returns {FeedbackWidget}
     */
    show()
    {
        this._element.classList.remove("feedbackwidget--hidden")
        this._element.classList.add('feedbackwidget--invisible')
        window.setTimeout(() => {
            this._element.classList.remove('feedbackwidget--invisible')
        }, 5)

        return this
    }


    /**
     * hide widget with fade animation
     * @returns {FeedbackWidget}
     */
    hide()
    {
        this._element.classList.add('feedbackwidget--invisible')
        window.setTimeout(() => {
            this._element.classList.remove('feedbackwidget--invisible')
            this._element.classList.add('feedbackwidget--hidden')
        }, 1000)
        return this
    }

    /**
     * @returns {boolean}
     */
    isShown()
    {
        return !this._element.classList.contains("feedbackwidget--hidden")
    }


    /**
     * toggle feedback visibility
     * @returns {FeedbackWidget}
      */
    toggle()
    {
        (this.isShown()) ? this.hide() : this.show()
        return this
    }


    /**
     * Event ran when accept, decline or close button is pressed
     */
    onClose()
    {
        this.hide()
        this.unlog()
    }


    /**
     * tada the given element infinitely
     * @param {HTMLElement} element
     * @returns {FeedbackWidget}
     */
    infiniteTada(element)
    {
        this.tada(element)
        window.setInterval(() =>
        {
            this.tada(element)
        }, 3000)
        return this
    }


    /**
     * tada the given element once
     * @param {HTMLElement} element
     * @returns {FeedbackWidget}
     */
    tada(element)
    {
        element.classList.add("tada")
        window.setTimeout(() => {
            element.classList.remove("tada")
        }, 1000)
        return this
    }


    /**
     * Log this feedback item
     * @returns {FeedbackWidget}
     */
    log()
    {
        FeedbackWidgetLogger.log(this._message, this._type)
        return this
    }


    /**
     * remove this feedback item from logs
     * @returns {FeedbackWidget}
     */
    unlog()
    {
        FeedbackWidgetLogger.remove(this._message, this._type)
        return this
    }
}
