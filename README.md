# Reversi Resources
By [Jasper van Veenhuizen](https://jasperv.dev/).

**School:** Windesheim University of Applied Sciences  
**Minor:** Web Development  
**Class:** Client Technology

This project contains the JavaScript, CSS and images for the Reversi Project.  
Which are then placed into, and served by, the Reversi MVC App.

- Resources that provide the MVC app with JavaScript, CSS and images can be found in the [current repository](https://gitlab.com/Putper/reversi-resources).
- MVC Application which is the game client, handling player info and serving the front-end can be found [here](https://gitlab.com/Putper/reversi-mvc).
- API handling game logic can be found [here](https://gitlab.com/Putper/reversi-api).


## Requirements
[node](https://nodejs.org/en/download/package-manager) for npm package manager.

Gulp for building the project:
```bash
npm install --global gulp-cli
```


## Setup
Open `./gulpfile.js/config.js` and change `localServerProjectPath`
to the `wwwroot` folder of the MVC (server) project.

Download dependencies:
```bash
npm install
```

Build project:
```bash
gulp build
```
All files are always built to both:
- the `dist` folder in this project
- the `www-root` folder, which you can set in `./gulpfile.js/config.js` by changing `localServerProjectPath`.


### Gulp tasks
Build JavaScript
```bash
gulp js
```

Build JavaScript Vendors
```bash
gulp js-vendor
```

Compile Sass
```bash
gulp sass
```

Build css Vendors
```bash
gulp css-vendor
```

Copy public files
```bash
gulp public
```

Build JavaScript, JS Vendor, CSS vendor, Public & Sass at once
```bash
gulp build
```

Automatically build when changes are made
```bash
gulp watch
```

## Credits
Bonsaiheldin | https://nora.la  
for grass tile textures
