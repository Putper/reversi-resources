module.exports = {
    localServerProjectPath : 'C:\\Users\\micro\\Desktop\\reversi\\mvc\\ReversiMvcApp\\wwwroot\\',
    files: {
        js: [
            'src/js/**/*.js',
            'src/js/*.js'
        ],
        jsOrder: [
            'src/js/Helpers.js',
            'src/js/Game.js',
            'src/js/FeedbackWidget.js',
            'src/js/Game.Data.js',
            'src/js/Game.API.js',
            'src/js/Game.Reversi.js',
            'src/js/Cat.js',
            'src/js/main.js'
        ],
        jsVendor: [
            'vendor/**/*.js',
            'vendor/*.js'
        ],
        sass: [
            'src/scss/**/*.scss',
            'src/scss/*.scss'
        ],
        cssVendor: [
            'vendor/**/*.css',
            'vendor/*.css'
        ],
        public: [
            'src/public/**'
        ],
        templates: [
            'src/templates/**/[^_]*.hbs'
        ],
        partialTemplates: [
            'src/templates/**/_*.hbs'
        ],
        dist: './dist'
    }
}