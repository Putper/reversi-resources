const {series, watch} = require('gulp')
const config = require('./config')
const merge = require('merge-stream')

// gulp js
const js = require('./tasks/js').js(config.files.js, config.files.jsOrder,config.localServerProjectPath)
js.displayName = 'js'

// gulp js vendor
const jsVendor = require('./tasks/js-vendor').jsVendor(config.files.jsVendor, config.localServerProjectPath)
jsVendor.displayName = 'js-vendor'

// gulp sass
const sass = require('./tasks/sass').sass(config.files.sass, config.localServerProjectPath)
sass.displayName = 'sass'

// gulp css vendor
const cssVendor = require('./tasks/css-vendor').cssVendor(config.files.cssVendor, config.localServerProjectPath)
cssVendor.displayName = 'css-vendor'

// gulp public
const publicFiles = require('./tasks/public').public(config.files.public, [config.localServerProjectPath, config.files.dist])
publicFiles.displayName = "public"

// gulp templates
const templates = require('./tasks/templates').templates(config.files.templates, config.files.partialTemplates, config.localServerProjectPath)
templates.displayName = 'templates'

// gulp build
const build = series(js, jsVendor, sass, cssVendor, publicFiles, templates)
build.displayName = "build"

// gulp watch
const watchFiles = () => {
    watch(config.files.js, series(js))
    watch(config.files.jsVendor, series(jsVendor))
    watch(config.files.sass, series(sass))
    watch(config.files.cssVendor, series(cssVendor))
    watch(config.files.public, series(publicFiles))
    watch(config.files.templates.concat(config.files.partialTemplates), series(templates))
}
watchFiles.displayName = "watch"


exports.default = build
exports.build = build
exports.watch = watchFiles
exports.js = js
exports.jsVendor = jsVendor
exports.sass = sass
exports.cssVendor = cssVendor
exports.public = publicFiles
exports.templates = templates
