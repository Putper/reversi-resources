const {src, dest} = require('gulp')
const gulpCopy = require('gulp-copy')
const merge = require('merge-stream')

/**
 *
 * @param {Array<string>} publicPath
 * @param {Array<string>} distPaths
 * @returns {function(): *}
 */
const public = function (publicPath, distPaths)
{
    return function () {
        let merged = merge()

        Array.from(distPaths).forEach((dist) =>
        {
            let result = src(publicPath)
                .pipe(gulpCopy(dist, {prefix:2}))
            merged.add(result)
        })

        return merged
    }
}

exports.public = public
