const {src, dest} = require('gulp')
const gulpSass = require('gulp-sass')(require('sass'))
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps')
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')

const sass = function (files_sass, backendPath)
{
    return function () {
        return src(files_sass)
            .pipe(gulpSass().on('error', gulpSass.logError))
            .pipe(sourcemaps.init())
            .pipe(concat('style.min.css'))
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
            .pipe(sourcemaps.write())
            .pipe(dest('./dist/css'))
            .pipe(dest(backendPath + 'css'))
    }
}

exports.sass = sass
