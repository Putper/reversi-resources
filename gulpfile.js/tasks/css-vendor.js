const {src, dest} = require('gulp')
const concat = require('gulp-concat')


const fn = function (vendorFiles, backendPath)
{
    return function () {
        return src(vendorFiles)
            .pipe(concat('vendor.css'))
            .pipe(dest('dist/css'))
            .pipe(dest(backendPath + 'css'))
    }
}

exports.cssVendor = fn
