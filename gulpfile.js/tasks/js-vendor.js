const {src, dest} = require('gulp')
const concat = require('gulp-concat')


const fn = function (vendorFiles, backendPath)
{
    return function () {
        return src(vendorFiles)
            .pipe(concat('vendor.js'))
            .pipe(dest('dist/js'))
            .pipe(dest(backendPath + 'js'))
    }
}

exports.jsVendor = fn
