const {src, dest} = require('gulp')
const order = require('gulp-order')
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')

const fn = function (jsFiles, jsFilesOrder, backendPath) {
    return function () {
        return src(jsFiles)
            .pipe(sourcemaps.init())
            .pipe(order(jsFilesOrder, {base: './'}))
            .pipe(concat('app.js'))
            .pipe(uglify())
            .pipe(sourcemaps.write())
            .pipe(dest('./dist/js'))
            .pipe(dest(backendPath + 'js'))
    }
}

exports.js = fn
