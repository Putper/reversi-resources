const {src, dest} = require('gulp')
const concat = require('gulp-concat')
const handlebars = require('gulp-handlebars')
const declare = require('gulp-declare')
const wrap = require('gulp-wrap')
const merge = require('merge-stream')
const path = require("path")


const templates = function (templateFiles, partialFiles, backendPath)
{
    return function () {
        const templates = src(templateFiles)
            .pipe(handlebars())
            // Wrap each template function in a call to Handlebars.template
            .pipe(wrap('Handlebars.template(<%= contents %>)'))
            // Declare template functions as properties and sub-properties of MyApp.templates
            .pipe(declare({
                namespace: 'spa_templates',
                noRedeclare: true, // Avoid duplicate declarations
                processName: function(filePath) {
                    // Allow nesting based on path using gulp-declare's processNameByPath()
                    // You can remove this option completely if you aren't using nested folders
                    // Drop the client/templates/ folder from the namespace path by removing it from the filePath
                    return declare.processNameByPath(filePath.replace('<parent_map>\\templates\\', ''))
                }
            }))


        const partials = src(partialFiles)
            .pipe(handlebars())
            .pipe(wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
                imports: {
                    processPartialName: function (fileName) {
                        // without underscore and extension
                        const file = path.basename(fileName, '.js').substr(1)
                        const directory = path.basename(path.dirname(fileName))
                        const partialName = directory + "." + file
                        // Escape with JSON.stringify
                        return JSON.stringify(partialName)
                    }
                }
            }))

        return merge(partials, templates)
            .pipe(concat('templates.js'))
            .pipe(dest('dist/js/'))
            .pipe(dest(backendPath+'js'))
    }
}

exports.templates = templates
